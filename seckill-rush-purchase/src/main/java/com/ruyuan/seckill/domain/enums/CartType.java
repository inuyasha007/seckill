package com.ruyuan.seckill.domain.enums;

/**
 * 购物车类型
 */
public enum CartType {

    /**
     * 表明是在购物车
     */
    CART,

    /**
     * 表明是在结算页
     */
    CHECKOUT,

    /**
     * 表明是在拼团
     */
    PINTUAN


}
