package com.ruyuan.seckill.domain.enums;

/**
 * 检查数据获取方式
 */
public enum CheckedWay {


    /**
     * 表明是在购物车进行获取
     */
    CART,

    /**
     * 表明是在立即购买进行获取
     */
    BUY_NOW,

}
