package com.ruyuan.seckill.domain.vo;

import com.ruyuan.seckill.domain.enums.QuantityType;

/**
 * 商品库存vo
 */
public class GoodsQuantityVO implements Cloneable {


    private Integer goodsId;

    private Integer skuId;

    private Integer quantity;

    private QuantityType quantityType;

    public GoodsQuantityVO() {
    }


    public GoodsQuantityVO(Integer goodsId, Integer skuId, Integer quantity) {
        super();
        this.goodsId = goodsId;
        this.skuId = skuId;
        this.quantity = quantity;

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Object object = super.clone();
        return object;
    }


    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getQuantity() {

        if (quantity == null) {
            return 0;
        }

        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public QuantityType getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(QuantityType quantityType) {
        this.quantityType = quantityType;
    }

    @Override
    public String toString() {
        return "GoodsQuantityVO{" +
                "goodsId=" + goodsId +
                ", skuId=" + skuId +
                ", quantity=" + quantity +
                ", quantityType=" + quantityType +
                '}';
    }
}

