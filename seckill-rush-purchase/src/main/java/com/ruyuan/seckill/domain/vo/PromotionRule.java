package com.ruyuan.seckill.domain.vo;

import com.ruyuan.seckill.domain.FullDiscountGiftDO;
import com.ruyuan.seckill.domain.enums.PromotionTarget;

import java.io.Serializable;

/**
 * Created by kingapex on 2018/12/12.
 */
public class PromotionRule implements Serializable {

    private static final long serialVersionUID = -5098604925079913257L;

    public PromotionRule(PromotionTarget target) {
        reducedTotalPrice = 0D;
        reducedPrice = 0D;
        pointGift = 0;
        usePoint = 0;
        freeShipping = false;
        tips = "";
        this.target = target;
        invalidReason = "";
        invalid = false;
    }

    /**
     * 一共要减多少钱
     */
    private Double reducedTotalPrice;

    /**
     * 单价减多少钱
     */
    private Double reducedPrice;

    private FullDiscountGiftDO goodsGift;
    private CouponVO couponGift;
    private Integer pointGift;
    private Integer usePoint;
    private CouponVO useCoupon;
    private Boolean freeShipping;
    private String tips;
    private PromotionTarget target;


    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * 促销标签
     */
    private String tag;

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    public String getInvalidReason() {
        return invalidReason;
    }

    public void setInvalidReason(String invalidReason) {
        this.invalidReason = invalidReason;
    }

    /**
     * 是否失效了
     */
    private boolean invalid;


    /**
     * 失效原因
     */
    private String invalidReason;


    public Double getReducedTotalPrice() {
        return reducedTotalPrice;
    }

    public void setReducedTotalPrice(Double reducedTotalPrice) {
        this.reducedTotalPrice = reducedTotalPrice;
    }

    public FullDiscountGiftDO getGoodsGift() {
        return goodsGift;
    }

    public void setGoodsGift(FullDiscountGiftDO goodsGift) {
        this.goodsGift = goodsGift;
    }

    public CouponVO getCouponGift() {
        return couponGift;
    }

    public void setCouponGift(CouponVO couponGift) {
        this.couponGift = couponGift;
    }

    public Integer getPointGift() {
        return pointGift;
    }

    public void setPointGift(Integer pointGift) {
        this.pointGift = pointGift;
    }

    public Integer getUsePoint() {
        return usePoint;
    }

    public void setUsePoint(Integer usePoint) {
        this.usePoint = usePoint;
    }

    public CouponVO getUseCoupon() {
        return useCoupon;
    }

    public void setUseCoupon(CouponVO useCoupon) {
        this.useCoupon = useCoupon;
    }

    public Boolean getFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(Boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public PromotionTarget getTarget() {
        return target;
    }

    public void setTarget(PromotionTarget target) {
        this.target = target;
    }

    public Double getReducedPrice() {
        return reducedPrice;
    }

    public void setReducedPrice(Double reducedPrice) {
        this.reducedPrice = reducedPrice;
    }

}
