package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.MemberCoupon;
import com.ruyuan.seckill.domain.vo.CartSkuVO;

/**
 * 优惠券计算器
 */
public interface CouponCalculator {

    /**
     * 计算商符合条件商品的总金额
     * @param coupon
     * @param sku
     * @return
     */
    Double calculate(MemberCoupon coupon, CartSkuVO sku);

}
